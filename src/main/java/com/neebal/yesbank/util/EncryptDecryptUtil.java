package com.neebal.yesbank.util;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class EncryptDecryptUtil {

	public static StringBuffer concatCipherWithKey(byte[] cipherText,
			byte[] iv, byte[] secretKey, byte[] partnerId) {

		StringBuffer stringBuffer = new StringBuffer();
		try {

			stringBuffer.append("data=");
			stringBuffer.append(Base64.getEncoder().encodeToString(cipherText));
			stringBuffer.append("&iv=");
			stringBuffer.append(Base64.getEncoder().encodeToString(iv));
			stringBuffer.append("&key=");
			stringBuffer.append(Base64.getEncoder().encodeToString(secretKey));
			stringBuffer.append("&partnerid=");
			stringBuffer.append(Base64.getEncoder().encodeToString(partnerId));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
		}
		return stringBuffer;
	}
	
	public static StringBuffer concatEncryptData(String algorithm,
			StringBuffer cipherMessage, String signature) {

		StringBuffer stringBuffer = new StringBuffer();
		try {

			stringBuffer.append(algorithm);
			stringBuffer.append(".");
			stringBuffer.append(cipherMessage);
			stringBuffer.append(".");
			stringBuffer.append(signature);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
		}
		return stringBuffer;
	}
	
	public static Map<String, String> extractContent(String payload) {

		Map<String, String> dataMap = new HashMap<String, String>();
		StringTokenizer st1 = new StringTokenizer(payload.toString(), ".");
		String algo = st1.nextToken();
		String content = st1.nextToken();
		String signature = st1.nextToken();
		dataMap.put("algo", algo);
		dataMap.put("content", content);
		dataMap.put("signature", signature);
		return dataMap;
	}

	public static Map<String, String> extractPayload(String content) {

		Map<String, String> dataMap = new HashMap<String, String>();
		for (String actualElement : content.split("&")) {

			if (actualElement.startsWith("data=")) {
				dataMap.put("data", actualElement.replace("data=", ""));
			} else if (actualElement.startsWith("iv=")) {
				dataMap.put("iv", actualElement.replace("iv=", ""));
			} else if (actualElement.startsWith("key=")) {
				dataMap.put("key", actualElement.replace("key=", ""));
			} else if (actualElement.startsWith("partnerid=")) {
				dataMap.put("partnerid",
						actualElement.replace("partnerid=", ""));
			}
		}
		return dataMap;
	}

	public static String extractSignature(String content) {
		// TODO Auto-generated method stub
		StringTokenizer st1 = new StringTokenizer(content.toString(), ".");
		st1.nextToken();
		st1.nextToken();
		String encryptedSignature = st1.nextToken();
		return encryptedSignature;
	}
}
