package com.neebal.yesbank.demo.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.Base64;

public class RsaSigner {

	//private KeyPair keyPair; 
	  public String signSHA256RSA(StringBuffer input/*, String strPk*/) throws Exception {
	        // Remove markers and new line characters in private key
	       /* String realPK = strPk.replaceAll("-----END PRIVATE KEY-----", "")
	                             .replaceAll("-----BEGIN PRIVATE KEY-----", "")
	                             .replaceAll("\n", "");
*/
	     //    keyPair = buildKeyPair();
	        /*byte[] b1 = Base64.getDecoder().decode(realPK);
	        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
	        KeyFactory kf = KeyFactory.getInstance("RSA");*/

	         String filename= "KeyPair/privateKey";
	        Signature privateSignature = Signature.getInstance("SHA256withRSA");
	        privateSignature.initSign(RsaEncryptor.getPrivateKey(filename));
	        privateSignature.update(input.toString().getBytes("UTF-8"));
	        byte[] s = privateSignature.sign();
	        return Base64.getEncoder().encodeToString(s);
	    }
	  
	  public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
	        final int keySize = 2048;
	        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
	        keyPairGenerator.initialize(keySize);      
	        return keyPairGenerator.genKeyPair();
	    }
	  
	  public boolean verifySHA256RSASignature(StringBuffer input, String signature, String extractedPartnerId/*, String strPk*/) throws Exception {
	        // Remove markers and new line characters in private key
	       /* String realPK = strPk.replaceAll("-----END PRIVATE KEY-----", "")
	                             .replaceAll("-----BEGIN PRIVATE KEY-----", "")
	                             .replaceAll("\n", "");
*/
	        /*byte[] b1 = Base64.getDecoder().decode(realPK);
	        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
	        KeyFactory kf = KeyFactory.getInstance("RSA");*/
	        
		  String filename= "KeyPair/publicKey"; // use the partner id to identify the public key
	        Signature publicSignature = Signature.getInstance("SHA256withRSA");
	        publicSignature.initVerify(RsaEncryptor.getPublicKey(filename));
	        publicSignature.update(input.toString().getBytes("UTF-8"));

	        byte[] signatureBytes = Base64.getDecoder().decode(signature);

	        return publicSignature.verify(signatureBytes);
	    }
}
