package com.neebal.yesbank.demo.security;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AesEncryptor {

	private SecureRandom secureRandom = new SecureRandom();
	private Cipher cipher;
	byte[] iv; //NEVER REUSE THIS IV WITH SAME KEY
	byte[] key;
	SecretKey secretKey;
	
	/*@Override
	public String decrypt(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String encrypt(String plainText) {
			}*/
	
	private SecretKey getRandomSecretKey() {
		
		key = new byte[32];
		secureRandom.nextBytes(key);
		 secretKey = new SecretKeySpec(key, "AES");
		return secretKey;
	}

	private byte[] getIntializationVector() {
		
		iv = new byte[12];
		secureRandom.nextBytes(iv);
		return iv;
	}
	
	private void initCipher() {
		
		
		try {
			cipher = Cipher.getInstance("AES/GCM/NoPadding");
			GCMParameterSpec parameterSpec = new GCMParameterSpec(128, getIntializationVector()); //128 bit auth tag length
			cipher.init(Cipher.ENCRYPT_MODE, getRandomSecretKey(), parameterSpec);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public byte[] decrypt(byte[] cipherText, byte[] decryptedIV, byte[] decryptedRandomNumber) {
		
		
		try {
			final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decryptedRandomNumber, "AES"), new GCMParameterSpec(128, decryptedIV));
			byte[] plainText= cipher.doFinal(cipherText);
			return plainText;
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	
	public byte[] encrypt(byte[] arg0) {
		initCipher();
		byte[] cipherText;
		try {
			cipherText = cipher.doFinal(arg0);
			return cipherText;

		} catch (IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	


	private byte[] getCypherText(byte[] cipherMessage) {
		ByteBuffer byteBuffer = ByteBuffer.wrap(cipherMessage);
		int ivLength = byteBuffer.getInt();
		if(ivLength < 12 || ivLength >= 16) { // check input parameter
		    throw new IllegalArgumentException("invalid iv length");
		}
		byte[] iv = new byte[ivLength];
		byteBuffer.get(iv);
		byte[] cipherText = new byte[byteBuffer.remaining()];
		byteBuffer.get(cipherText);
		return cipherText;
	}
	

	/*private byte[] wrapKey(PublicKey pubKey, SecretKey symKey)
	        throws InvalidKeyException, IllegalBlockSizeException {
	    try {
	        final Cipher cipher = Cipher
	                .getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
	        cipher.init(Cipher.WRAP_MODE, pubKey);
	        final byte[] wrapped = cipher.wrap(symKey);
	        return wrapped;
	    } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
	        throw new IllegalStateException(
	                "Java runtime does not support RSA/ECB/OAEPWithSHA1AndMGF1Padding",
	                e);
	    }
	}*/

	public byte[] getIv() {
		return iv;
	}

	public byte[] getKey() {
		return key;
	}


	
}
