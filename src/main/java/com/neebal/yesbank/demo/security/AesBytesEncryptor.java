package com.neebal.yesbank.demo.security;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.BytesKeyGenerator;
import org.springframework.security.crypto.keygen.KeyGenerators;

public class AesBytesEncryptor /*implements TextEncryptor */{
/*
	private final SecretKey secretKey;

	private final Cipher encryptor;

	private final Cipher decryptor;

	private final BytesKeyGenerator ivGenerator;

	private CipherAlgorithm alg;

	//private static final String AES_CBC_ALGORITHM = "AES/CBC/PKCS5Padding";

	private static final String AES_GCM_ALGORITHM = "AES/GCM/NoPadding";

	public enum CipherAlgorithm {

		CBC(AES_CBC_ALGORITHM, NULL_IV_GENERATOR), GCM(AES_GCM_ALGORITHM, KeyGenerators
				.secureRandom(16));

		private BytesKeyGenerator ivGenerator;
		private String name;

		private CipherAlgorithm(String name, BytesKeyGenerator ivGenerator) {
			this.name = name;
			this.ivGenerator = ivGenerator;
		}

		@Override
		public String toString() {
			return this.name;
		}

		public AlgorithmParameterSpec getParameterSpec(byte[] iv) {
			return this == CBC ? new IvParameterSpec(iv) : new GCMParameterSpec(128, iv);
		}

		public Cipher createCipher() {
			return newCipher(this.toString());
		}

		public BytesKeyGenerator defaultIvGenerator() {
			return this.ivGenerator;
		}
	}

	public AesBytesEncryptor(String password, CharSequence salt) {
		this(password, salt, null);
	}

	public AesBytesEncryptor(String password, CharSequence salt,
			BytesKeyGenerator ivGenerator) {
		this(password, salt, ivGenerator, CipherAlgorithm.CBC);
	}

	public AesBytesEncryptor(String password, CharSequence salt,
			BytesKeyGenerator ivGenerator, CipherAlgorithm alg) {
		PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), Hex.decode(salt),
				1024, 256);
		SecretKey secretKey = newSecretKey("PBKDF2WithHmacSHA1", keySpec);
		this.secretKey = new SecretKeySpec(secretKey.getEncoded(), "AES");
		this.alg = alg;
		this.encryptor = alg.createCipher();
		this.decryptor = alg.createCipher();
		this.ivGenerator = ivGenerator != null ? ivGenerator : alg.defaultIvGenerator();
	}

	@Override
	public String decrypt(String arg0) {

		synchronized (this.encryptor) {
			byte[] iv = this.ivGenerator.generateKey();
			initCipher(this.encryptor, Cipher.ENCRYPT_MODE, this.secretKey,
					this.alg.getParameterSpec(iv));
			byte[] encrypted = doFinal(this.encryptor, bytes);
			return this.ivGenerator != NULL_IV_GENERATOR ? concatenate(iv, encrypted)
					: encrypted;
		}
		}

	@Override
	public String encrypt(String arg0) {
		synchronized (this.decryptor) {
			byte[] iv = iv(encryptedBytes);
			initCipher(this.decryptor, Cipher.DECRYPT_MODE, this.secretKey,
					this.alg.getParameterSpec(iv));
			return doFinal(
					this.decryptor,
					this.ivGenerator != NULL_IV_GENERATOR ? encrypted(encryptedBytes,
							iv.length) : encryptedBytes);
		}
	}
	

	// internal helpers

	private byte[] iv(byte[] encrypted) {
		return this.ivGenerator != NULL_IV_GENERATOR ? subArray(encrypted, 0,
				this.ivGenerator.getKeyLength()) : NULL_IV_GENERATOR.generateKey();
	}

	private byte[] encrypted(byte[] encryptedBytes, int ivLength) {
		return subArray(encryptedBytes, ivLength, encryptedBytes.length);
	}

	private static final BytesKeyGenerator NULL_IV_GENERATOR = new BytesKeyGenerator() {

		private final byte[] VALUE = new byte[16];

		public int getKeyLength() {
			return this.VALUE.length;
		}

		public byte[] generateKey() {
			return this.VALUE;
		}

	};*/

	

}
