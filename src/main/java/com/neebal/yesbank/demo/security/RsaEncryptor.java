package com.neebal.yesbank.demo.security;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class RsaEncryptor {

	
	 public byte[] decrypt(PrivateKey privateKey, byte[] message) throws Exception {
		 	//PrivateKey privateKeyy = getPrivate("Certificates/partner.key");
		 
	        Cipher cipher = Cipher.getInstance("RSA");  
	        cipher.init(Cipher.DECRYPT_MODE, privateKey);  

	        return cipher.doFinal(message);  
	    }
	    
	    public byte[] encrypt(PublicKey publicKey, byte [] encrypted) throws Exception {
	        Cipher cipher = Cipher.getInstance("RSA");  
	        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
	        
	        return cipher.doFinal(encrypted);
	    }

	    public static PublicKey getPublicKey(String filename) {

			try {
				X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(
						readFileBytes(filename));
				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				return keyFactory.generatePublic(publicSpec);
			} catch (NoSuchAlgorithmException
					| InvalidKeySpecException e) {
				
			}

			return null;
		}

		public static PrivateKey getPrivateKey(String filename) {
			try {
				PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
						readFileBytes(filename));
				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				return keyFactory.generatePrivate(keySpec);
			} catch (NoSuchAlgorithmException
					| InvalidKeySpecException e) {
				
			}
			return null;
		}

		public static byte[] readFileBytes(String filename){
			try {
				Path path = Paths.get(filename);
				return Files.readAllBytes(path);
			} catch (IOException e) {
				System.out.println();
			}
			return null;
		}

		public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
	        final int keySize = 2048;
	        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
	        keyPairGenerator.initialize(keySize);      
	        return keyPairGenerator.genKeyPair();
	    }
		/*
		public PrivateKey getPrivate(String filename) throws Exception {
			byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			return kf.generatePrivate(spec);
		}

		// https://docs.oracle.com/javase/8/docs/api/java/security/spec/X509EncodedKeySpec.html
		public PublicKey getPublic(String filename) throws Exception {
			byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
			X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			return kf.generatePublic(spec);
		}*/
}
