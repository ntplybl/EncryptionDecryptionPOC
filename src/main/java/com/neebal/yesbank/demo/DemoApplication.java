package com.neebal.yesbank.demo;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Map;

import org.json.JSONObject;

import com.neebal.yesbank.demo.security.AesEncryptor;
import com.neebal.yesbank.demo.security.RsaEncryptor;
import com.neebal.yesbank.demo.security.RsaSigner;
import com.neebal.yesbank.util.EncryptDecryptUtil;

//@SpringBootApplication
public class DemoApplication {

	private static RsaEncryptor rsaEncryptor;
	//private static KeyPair keyPair;
	private static RsaSigner rsaSigner;
	
	public static void main(String[] args) {
		
		
		/******Transpure*************/
			
		
		String message = "testMessage";
		String algorithm = "RSA";
		
			System.out.println("/**************************************************************************************/\n\n\n");
			System.out.println("Message to be encrypted: \n"+message);
			byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes());
			
			StringBuffer cipherMessage = encrypt(encodedBytes, "1");
			System.out.println("/**************************************************************************************/\n\n\n");
			System.out.println("Encrypted Message: "+cipherMessage);
			
			/**************Sign using private key of Transpure************/
			rsaSigner = new RsaSigner();
			String signature = signCipherMessage(cipherMessage);
			/**************Sign using private key of Transpure************/
		
			StringBuffer concatEncryptData = EncryptDecryptUtil.concatEncryptData(algorithm,cipherMessage,signature);
			JSONObject jsonResultObj = new JSONObject();
			jsonResultObj.put("content", concatEncryptData);

		/******Transpure*************/
		
		
		/******Partner*************/
			
			String content = jsonResultObj.get("content").toString();
			Map<String, String> extractContent = EncryptDecryptUtil.extractContent(content);
			
			if(extractContent.size() == 3){
				Map<String, String> extractPayload = EncryptDecryptUtil.extractPayload(extractContent.get("content"));
				
				if(extractPayload.size() == 4){
					String extractedSignature = extractContent.get("signature");
					StringBuffer extractedCipherMessage = new StringBuffer();
					extractedCipherMessage.append(extractContent.get("content"));
					
					String decryptedMessage = null ;
					String extractPartnerId = decryptPartnerId(extractPayload.get("partnerid"));
					
					if(extractPartnerId != null && extractPartnerId != ""){
						boolean isVerified = verifySignature(extractedCipherMessage , extractedSignature, extractPartnerId);
						if(isVerified) {
							decryptedMessage = decrypt(extractPayload);
							System.out.println("Decrypted Message: \n"+decryptedMessage);
						}else {
							System.out.println("Message source is not trusted");
						}
					}
					
				}else{
					System.out.println("Something gone wrong...");
				}
			}else{
				System.out.println("Something gone wrong...");
			}
			
			System.out.println("/**************************************************************************************/\n\n\n");

		/******Partner*************/
	
	}
	
	public static boolean verifySignature(StringBuffer cipherMessage,
			String signature, String extractPartnerId) {
		
		try {
			return rsaSigner.verifySHA256RSASignature(cipherMessage, signature,
					extractPartnerId);
		} catch (Exception e) {
			
		}
		return false;

	}

	private static String signCipherMessage(StringBuffer cipherMessage) {
		
		
		try {
			return rsaSigner.signSHA256RSA(cipherMessage);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return null;
		
	}

/*	private static String decrypt(StringBuffer cipherMessage) {
	
		StringTokenizer st1 =
	             new StringTokenizer(cipherMessage.toString(), ".");
		String cipherText = st1.nextToken();
		String encryptedIV = st1.nextToken();
		String encryptedSecretKey = st1.nextToken();
		
		byte[] decryptedIV = rsaDecryptor(Base64.getDecoder().decode(encryptedIV));
		byte[] decryptedSecretKey = rsaDecryptor(Base64.getDecoder().decode(encryptedSecretKey));
		
		AesEncryptor aesEncryptor= new AesEncryptor();
		byte[] dbs = aesEncryptor.decrypt(Base64.getDecoder().decode(cipherText), decryptedIV, decryptedSecretKey);
		byte[] decodedBytes = Base64.getDecoder().decode(dbs);
		String decrypted = new String(decodedBytes);
		return decrypted;
	}
	*/
	public static String decrypt(Map<String, String> extractPayload) {

		String cipherText = extractPayload.get("data");
		String encryptedIV = extractPayload.get("iv");
		String encryptedSecretKey = extractPayload.get("key");
		String encryptedPartnerId = extractPayload.get("partnerid");

		byte[] decryptedIV = rsaDecryptor(Base64.getDecoder().decode(encryptedIV));
		byte[] decryptedSecretKey = rsaDecryptor(Base64.getDecoder().decode(encryptedSecretKey));
		rsaDecryptor(Base64.getDecoder().decode(encryptedPartnerId));

		AesEncryptor aesEncryptor = new AesEncryptor();
		byte[] dbs = aesEncryptor.decrypt(Base64.getDecoder().decode(cipherText),
				decryptedIV, decryptedSecretKey);
		byte[] decodedBytes = Base64.getDecoder().decode(dbs);
		String decrypted = new String(decodedBytes);
		return decrypted;
	}

	private static StringBuffer encrypt(byte[] encodedBytes, String partnerId) {
		AesEncryptor aesEncryptor= new AesEncryptor();
		
		
		/************************AES Encryption*****************************/
		byte[] cypherText = aesEncryptor.encrypt(encodedBytes);
		/************************AES Encryption*****************************/
		
		
		
		/******************************RSA**********************************/
		rsaEncryptor = new RsaEncryptor();
		/*try {
			keyPair = rsaEncryptor.buildKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}*/
		byte[] encryptedRandomNumber = rsaEncryptor(aesEncryptor.getKey());
		byte[] encryptedIV = rsaEncryptor(aesEncryptor.getIv());
		byte[] encryptedPartnerId = rsaEncryptor(partnerId.getBytes());
		/******************************RSA**********************************/
		
		
		/*****************************Concatenation of Cypher Text, IV and Random Number******************/
		StringBuffer cipherMessage = EncryptDecryptUtil.concatCipherWithKey(cypherText, encryptedIV, encryptedRandomNumber, encryptedPartnerId);
		/*****************************Concatenation of Cypher Text, IV and Random Number******************/
		
		
		return cipherMessage;
	}
	
	private static byte[] rsaEncryptor(byte[] key) {
	
		try {
	
			String filename = "KeyPair/publicKey";  // use the partner id to identify the public key
		    PublicKey publicKey = RsaEncryptor.getPublicKey(filename);
		    //PublicKey publicKey = keyPair.getPublic();
		    //writeToFile("KeyPair/publicKey", publicKey.getEncoded());
		   
			byte[] encryptedKey;
			encryptedKey = rsaEncryptor.encrypt(publicKey, key);
			return encryptedKey;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static byte[] rsaDecryptor(byte[] key) {
		
		try {
	
			String filename = "KeyPair/privateKey";
			PrivateKey privateKey = RsaEncryptor.getPrivateKey(filename);
		    //PrivateKey privateKey = keyPair.getPrivate();
		   
			byte[] encryptedKey;
			encryptedKey = rsaEncryptor.decrypt(privateKey, key);
			return encryptedKey;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
/*	
	private static StringBuffer concatCipherWithKey(byte[] cipherText, byte[] iv, byte[] secretKey) {
		

		ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + cipherText.length);
		byteBuffer.putInt(iv.length);
		byteBuffer.put(iv);byteBuffer.getInt();
		byteBuffer.putInt(secretKey.length);
		byteBuffer.put(cipherText);
		byte[] cipherMessage = byteBuffer.array();
		//return cipherMessage;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(Base64.getEncoder().encodeToString(cipherText));
		stringBuffer.append(".");
		stringBuffer.append(Base64.getEncoder().encodeToString(iv));
		stringBuffer.append(".");
		stringBuffer.append(Base64.getEncoder().encodeToString(secretKey));
		
		return stringBuffer;
	}
	*/


	public static String decryptPartnerId(String encryptedPartnerId) {

		byte[] decryptedPartnerId = rsaDecryptor(Base64.getDecoder().decode(encryptedPartnerId));

		String decrypted = new String(decryptedPartnerId);
		return decrypted;
	}
	
//	public static void writeToFile(String path, byte[] key) throws IOException {
//		File f = new File(path);
//		f.getParentFile().mkdirs();
//
//		FileOutputStream fos = new FileOutputStream(f);
//		fos.write(key);
//		fos.flush();
//		fos.close();
//	}
	
	/*private static byte[] getCypherText(byte[] cipherMessage) {
		ByteBuffer byteBuffer = ByteBuffer.wrap(cipherMessage);
		int ivLength = byteBuffer.getInt();
		if(ivLength < 12 || ivLength >= 16) { // check input parameter
		    throw new IllegalArgumentException("invalid iv length");
		}
		byte[] iv = new byte[ivLength];
		byteBuffer.get(iv);
		byte[] cipherText = new byte[byteBuffer.remaining()];
		byteBuffer.get(cipherText);
		return cipherText;
	}*/
}